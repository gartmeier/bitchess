# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2024-07-21

### Added
whole game logic:
- system for making and accepting game requests [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)
- Game class wich contains the board representation, move generation and execution which chesslib under the hood [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)
- GameManager class which manages multiple games and performs actions via names and/or game IDs [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)
- return type system of the MatchMaker(request handling) and GameManager which indicates the outcome of an operation [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)

## [0.2.0] - 2024-07-20

### Added
- BoardVisualizer which transforms FEN notation to ascii board representation [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)
- tests for the above [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)

## [0.1.0] - 2024-07-20

### Added
- runner for this specific gitlab repository to execute the pipelines [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)

### Changed
- git, gitlab and gradle configuration to an initial state [@Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)

