package bitchess.visual;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link BoardVisualizer}. */
public class BoardVisualizerTest {

  @Test
  public void testFenFromWhitePerspective() {
    String fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    String asciiBoard = BoardVisualizer.fenToAsciiBoard(fen);
    String expectedAsciiBoard =
        """
                    +----+----+----+----+----+----+----+----+
                    | r  | n  | b  | q  | k  | b  | n  | r  |  8
                    +----+----+----+----+----+----+----+----+
                    | p  | p  | p  | p  | p  | p  | p  | p  |  7
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  6
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  5
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  4
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  3
                    +----+----+----+----+----+----+----+----+
                    | P  | P  | P  | P  | P  | P  | P  | P  |  2
                    +----+----+----+----+----+----+----+----+
                    | R  | N  | B  | Q  | K  | B  | N  | R  | 1
                    +----+----+----+----+----+----+----+----+
                      a    b    c    d    e    f    g    h
                      """;
    Assertions.assertEquals(expectedAsciiBoard, asciiBoard);

    fen = "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2 ";
    asciiBoard = BoardVisualizer.fenToAsciiBoard(fen);
    expectedAsciiBoard =
        """
                    +----+----+----+----+----+----+----+----+
                    | r  | n  | b  | q  | k  | b  | n  | r  |  8
                    +----+----+----+----+----+----+----+----+
                    | p  | p  |    | p  | p  | p  | p  | p  |  7
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  6
                    +----+----+----+----+----+----+----+----+
                    |    |    | p  |    |    |    |    |    |  5
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    | P  |    |    |    |  4
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    | N  |    |    |  3
                    +----+----+----+----+----+----+----+----+
                    | P  | P  | P  | P  |    | P  | P  | P  |  2
                    +----+----+----+----+----+----+----+----+
                    | R  | N  | B  | Q  | K  | B  |    | R  | 1
                    +----+----+----+----+----+----+----+----+
                      a    b    c    d    e    f    g    h
                      """;
    Assertions.assertEquals(expectedAsciiBoard, asciiBoard);
  }

  @Test
  public void testFenFromBlackPerspective() {
    String fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    String asciiBoard = BoardVisualizer.fenToAsciiBoard(fen, BoardVisualizer.Perspective.BLACK);
    String expectedAsciiBoard =
        """
            +----+----+----+----+----+----+----+----+
            | R  | N  | B  | K  | Q  | B  | N  | R  |  1
            +----+----+----+----+----+----+----+----+
            | P  | P  | P  | P  | P  | P  | P  | P  |  2
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  3
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  4
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  5
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  6
            +----+----+----+----+----+----+----+----+
            | p  | p  | p  | p  | p  | p  | p  | p  |  7
            +----+----+----+----+----+----+----+----+
            | r  | n  | b  | k  | q  | b  | n  | r  | 8
            +----+----+----+----+----+----+----+----+
              h    g    f    e    d    c    b    a
            """;
    Assertions.assertEquals(expectedAsciiBoard, asciiBoard);

    fen = "rn1q1bnr/p1pkppp1/3pb3/1p5p/P5Q1/N3P3/1PPP1PPP/R1BK1BNR w - - 4 6";
    asciiBoard = BoardVisualizer.fenToAsciiBoard(fen, BoardVisualizer.Perspective.BLACK);
    expectedAsciiBoard =
        """
            +----+----+----+----+----+----+----+----+
            | R  | N  | B  |    | K  | B  |    | R  |  1
            +----+----+----+----+----+----+----+----+
            | P  | P  | P  |    | P  | P  | P  |    |  2
            +----+----+----+----+----+----+----+----+
            |    |    |    | P  |    |    |    | N  |  3
            +----+----+----+----+----+----+----+----+
            |    | Q  |    |    |    |    |    | P  |  4
            +----+----+----+----+----+----+----+----+
            | p  |    |    |    |    |    | p  |    |  5
            +----+----+----+----+----+----+----+----+
            |    |    |    | b  | p  |    |    |    |  6
            +----+----+----+----+----+----+----+----+
            |    | p  | p  | p  | k  | p  |    | p  |  7
            +----+----+----+----+----+----+----+----+
            | r  | n  | b  |    | q  |    | n  | r  | 8
            +----+----+----+----+----+----+----+----+
              h    g    f    e    d    c    b    a
            """;
    Assertions.assertEquals(expectedAsciiBoard, asciiBoard);
  }

  @Test
  public void testFenFromEitherPerspective() {
    String fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    String asciiBoard =
        BoardVisualizer.fenToAsciiBoard(fen, BoardVisualizer.Perspective.CURRENT_PLAYER);
    String expectedAsciiBoard =
        """
            +----+----+----+----+----+----+----+----+
            | r  | n  | b  | q  | k  | b  | n  | r  |  8
            +----+----+----+----+----+----+----+----+
            | p  | p  | p  | p  | p  | p  | p  | p  |  7
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  6
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  5
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  4
            +----+----+----+----+----+----+----+----+
            |    |    |    |    |    |    |    |    |  3
            +----+----+----+----+----+----+----+----+
            | P  | P  | P  | P  | P  | P  | P  | P  |  2
            +----+----+----+----+----+----+----+----+
            | R  | N  | B  | Q  | K  | B  | N  | R  | 1
            +----+----+----+----+----+----+----+----+
              a    b    c    d    e    f    g    h
            """;
    Assertions.assertEquals(expectedAsciiBoard, asciiBoard);

    fen = "q4b1r/2pnppp1/r2pk2n/pp5p/P5P1/N1P1P3/RP1P1PBP/1KB3NR b - - 4 12";
    asciiBoard = BoardVisualizer.fenToAsciiBoard(fen, BoardVisualizer.Perspective.CURRENT_PLAYER);
    expectedAsciiBoard =
        """
            +----+----+----+----+----+----+----+----+
            | R  | N  |    |    |    | B  | K  |    |  1
            +----+----+----+----+----+----+----+----+
            | P  | B  | P  |    | P  |    | P  | R  |  2
            +----+----+----+----+----+----+----+----+
            |    |    |    | P  |    | P  |    | N  |  3
            +----+----+----+----+----+----+----+----+
            |    | P  |    |    |    |    |    | P  |  4
            +----+----+----+----+----+----+----+----+
            | p  |    |    |    |    |    | p  | p  |  5
            +----+----+----+----+----+----+----+----+
            | n  |    |    | k  | p  |    |    | r  |  6
            +----+----+----+----+----+----+----+----+
            |    | p  | p  | p  | n  | p  |    |    |  7
            +----+----+----+----+----+----+----+----+
            | r  |    | b  |    |    |    |    | q  | 8
            +----+----+----+----+----+----+----+----+
              h    g    f    e    d    c    b    a
              """;
    Assertions.assertEquals(expectedAsciiBoard, asciiBoard);
  }
}
