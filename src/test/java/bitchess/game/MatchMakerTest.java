package bitchess.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link MatchMaker}. */
public class MatchMakerTest {

  @Test
  public void testAddingRequests() {
    MatchMaker matchMaker = new MatchMaker();

    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("a", "b"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.ALREADY_EXISTS, matchMaker.privateRequest("a", "b"));

    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.publicRequest("a"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.ALREADY_EXISTS, matchMaker.publicRequest("a"));

    Assertions.assertEquals(
        MatchMaker.OperationStatus.FROM_EQUALS_TO, matchMaker.privateRequest("a", "a"));

    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("c", "b"));

    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.publicRequest("b,a"));
  }

  @Test
  public void testAcceptingRequests() {
    MatchMaker matchMaker = new MatchMaker();

    Assertions.assertEquals(
        MatchMaker.OperationStatus.FROM_EQUALS_TO, matchMaker.privateRequest("a", "a"));

    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("a", "b"));
    // accept private request
    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.accept("a", "b"));
    // request should have been removed
    Assertions.assertEquals(MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.accept("a", "b"));

    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.publicRequest("a"));
    // accept public reuqest from any other player
    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.accept("a", "randomPLayerX"));
    // request should have been removed
    Assertions.assertEquals(
        MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.accept("a", "randomPLayerY"));

    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("a", "b"));
    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.publicRequest("a"));

    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.accept("a", "b"));
    // private request should be picked over public request
    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("a", "b"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.ALREADY_EXISTS, matchMaker.publicRequest("a"));

    // trying to accept a private request to wrong player
    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("d", "e"));
    Assertions.assertEquals(MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.accept("d", "f"));
  }

  @Test
  public void testDecliningRequests() {
    MatchMaker matchMaker = new MatchMaker();

    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("a", "b"));

    // only b can decline
    Assertions.assertEquals(MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.decline("a", "c"));
    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.decline("a", "b"));
    // request should have been removed
    Assertions.assertEquals(MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.decline("a", "b"));

    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.publicRequest("a"));
    // public requests cant be declined
    Assertions.assertEquals(MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.decline("a", "b"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.ALREADY_EXISTS, matchMaker.publicRequest("a"));
  }

  @Test
  public void testTakingBackRequest() {
    MatchMaker matchMaker = new MatchMaker();
    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("a", "b"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.takeBackPrivate("a", "b"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.takeBackPrivate("a", "b"));

    Assertions.assertEquals(
        MatchMaker.OperationStatus.SUCCESS, matchMaker.privateRequest("a", "b"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.takeBackPrivate("b", "a"));

    Assertions.assertEquals(MatchMaker.OperationStatus.SUCCESS, matchMaker.publicRequest("a"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.NOT_EXISTS, matchMaker.takeBackPrivate("a", "c"));
    Assertions.assertEquals(
        MatchMaker.OperationStatus.ALREADY_EXISTS, matchMaker.publicRequest("a"));
  }
}
