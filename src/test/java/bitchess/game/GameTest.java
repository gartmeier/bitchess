package bitchess.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link Game}. */
public class GameTest {

  @Test
  public void testWinnigGame() {
    // all correct FENs were generated with stockfish

    Game game = new Game("1", "playerA", "playerB");

    Assertions.assertEquals(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", game.getFen());
    Assertions.assertTrue(game.isTurnOf("playerA"));
    String expectedBoard =
        """
                    +----+----+----+----+----+----+----+----+
                    | r  | n  | b  | q  | k  | b  | n  | r  |  8
                    +----+----+----+----+----+----+----+----+
                    | p  | p  | p  | p  | p  | p  | p  | p  |  7
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  6
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  5
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  4
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  3
                    +----+----+----+----+----+----+----+----+
                    | P  | P  | P  | P  | P  | P  | P  | P  |  2
                    +----+----+----+----+----+----+----+----+
                    | R  | N  | B  | Q  | K  | B  | N  | R  | 1
                    +----+----+----+----+----+----+----+----+
                      a    b    c    d    e    f    g    h
                      """;
    Assertions.assertEquals(expectedBoard, game.getAsciiBoard());

    Assertions.assertFalse(game.doMove("Ke4"));
    Assertions.assertTrue(game.doMove("e4"));

    expectedBoard =
        """
                    +----+----+----+----+----+----+----+----+
                    | r  | n  | b  | q  | k  | b  | n  | r  |  8
                    +----+----+----+----+----+----+----+----+
                    | p  | p  | p  | p  | p  | p  | p  | p  |  7
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  6
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  5
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    | P  |    |    |    |  4
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  3
                    +----+----+----+----+----+----+----+----+
                    | P  | P  | P  | P  |    | P  | P  | P  |  2
                    +----+----+----+----+----+----+----+----+
                    | R  | N  | B  | Q  | K  | B  | N  | R  | 1
                    +----+----+----+----+----+----+----+----+
                      a    b    c    d    e    f    g    h
                      """;
    Assertions.assertEquals(expectedBoard, game.getAsciiBoard());

    Assertions.assertTrue(game.isTurnOf("playerB"));
    Assertions.assertEquals(Game.GameStatus.RUNNING, game.getGameStatus());
    Assertions.assertEquals(
        "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1", game.getFen());

    Assertions.assertTrue(game.doMove("e5"));
    Assertions.assertTrue(game.doMove("Bc4"));
    Assertions.assertTrue(game.doMove("a5"));
    Assertions.assertTrue(game.doMove("Qf3"));
    Assertions.assertTrue(game.doMove("a4"));
    Assertions.assertTrue(game.doMove("Qxf7#"));

    // should be a checkmate
    Assertions.assertEquals(Game.GameStatus.WHITE_WIN, game.getGameStatus());

    expectedBoard =
        """
                    +----+----+----+----+----+----+----+----+
                    | r  | n  | b  | q  | k  | b  | n  | r  |  8
                    +----+----+----+----+----+----+----+----+
                    |    | p  | p  | p  |    | Q  | p  | p  |  7
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  6
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    | p  |    |    |    |  5
                    +----+----+----+----+----+----+----+----+
                    | p  |    | B  |    | P  |    |    |    |  4
                    +----+----+----+----+----+----+----+----+
                    |    |    |    |    |    |    |    |    |  3
                    +----+----+----+----+----+----+----+----+
                    | P  | P  | P  | P  |    | P  | P  | P  |  2
                    +----+----+----+----+----+----+----+----+
                    | R  | N  | B  |    | K  |    | N  | R  | 1
                    +----+----+----+----+----+----+----+----+
                      a    b    c    d    e    f    g    h
                      """;

    Assertions.assertEquals(expectedBoard, game.getAsciiBoard());
    Assertions.assertEquals(
        "rnbqkbnr/1ppp1Qpp/8/4p3/p1B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4", game.getFen());
  }

  @Test
  public void testRepetition() {
    Game game =
        new Game(
            "1",
            "playerA",
            "playerB",
            "1nbqk1nr/1pppppbp/r5p1/8/4P1Q1/1P5N/P1PP1PPP/RNB1K2R b KQk - 2 5");

    Assertions.assertEquals(Game.GameStatus.RUNNING, game.getGameStatus());
    Assertions.assertTrue(game.isTurnOf("playerB"));

    game.doMove("Qg4");
    game.doMove("Ra7");
    game.doMove("Qh4");
    game.doMove("Ra6");
    game.doMove("Qg4");
    game.doMove("Ra7");
    game.doMove("Qh4");
    game.doMove("Ra6");
    game.doMove("Qg4");
    game.doMove("Ra7");
    game.doMove("Qh4");
    game.doMove("Ra6");

    Assertions.assertEquals(Game.GameStatus.REPETITION, game.getGameStatus());
  }

  @Test
  public void testStaleMate() {
    Game game =
        new Game("1", "playerA", "playerB", "k6N/3Q4/8/3P4/8/2PP3P/P4PP1/RNB1K2R w KQ - 1 23");

    Assertions.assertEquals(Game.GameStatus.RUNNING, game.getGameStatus());

    game.doMove("Qc7");

    Assertions.assertEquals(Game.GameStatus.STALEMATE, game.getGameStatus());
  }

  @Test
  public void testDraw() {
    Game game = new Game("1", "playerA", "playerB", "8/8/8/8/2K3P1/7k/8/8 w - - 0 43");
    Assertions.assertEquals(Game.GameStatus.RUNNING, game.getGameStatus());

    game.doMove("Kd5");
    game.doMove("Kxg4");

    Assertions.assertEquals(Game.GameStatus.DRAW, game.getGameStatus());
  }
}
