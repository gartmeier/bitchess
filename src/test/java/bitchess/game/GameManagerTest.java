package bitchess.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link GameManager}. */
public class GameManagerTest {

  /** Test for {@link GameManager}. */
  @Test
  public void testAddingGames() {
    GameManager gameManager = new GameManager();

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("1", "a", "b"));

    // same game id not allowed
    Assertions.assertEquals(
        GameManager.OperationStatus.MULTIPLE_GAMES, gameManager.addGame("1", "x", "y"));
    // but same names are
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("2", "a", "b"));
  }

  @Test
  public void testForNoGame() {
    GameManager gameManager = new GameManager();

    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGameById("1").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGameByPlayer("a").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGame("a", "1").getFirst());
    Assertions.assertEquals(GameManager.OperationStatus.NO_GAME, gameManager.doMove("a", "lol"));
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.doMove("a", "1", "lol"));
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGameStringsByPlayer("a").getFirst());

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("1", "a", "b"));

    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGameById("2").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGameByPlayer("x").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGame("x", "2").getFirst());
    Assertions.assertEquals(GameManager.OperationStatus.NO_GAME, gameManager.doMove("x", "lol"));
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.doMove("a", "2", "lol"));
    Assertions.assertEquals(
        GameManager.OperationStatus.NO_GAME, gameManager.getGameStringsByPlayer("x").getFirst());
    Assertions.assertEquals(GameManager.OperationStatus.NO_GAME, gameManager.dropGame("x"));
  }

  @Test
  public void testForMultipleGames() {
    GameManager gameManager = new GameManager();

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("1", "a", "b"));
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("2", "b", "a"));

    Assertions.assertEquals(
        GameManager.OperationStatus.MULTIPLE_GAMES, gameManager.getGameByPlayer("a").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.MULTIPLE_GAMES, gameManager.doMove("a", "lol"));
    Assertions.assertEquals(GameManager.OperationStatus.MULTIPLE_GAMES, gameManager.dropGame("a"));

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getIdsByPlayer("a").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getIdsByPlayer("b").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getGameStringsByPlayer("a").getFirst());
  }

  @Test
  public void testWithSingleGame() {
    GameManager gameManager = new GameManager();

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("1", "a", "b"));

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getGameByPlayer("a").getFirst());
    Assertions.assertEquals(GameManager.OperationStatus.SUCCESS, gameManager.doMove("a", "e4"));
    Assertions.assertEquals(
        GameManager.OperationStatus.NOT_YOUR_TURN, gameManager.doMove("a", "e4"));

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getIdsByPlayer("a").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getIdsByPlayer("b").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getGameStringsByPlayer("a").getFirst());

    Assertions.assertEquals(GameManager.OperationStatus.SUCCESS, gameManager.dropGame("a"));
  }

  @Test
  public void testMultipleGamesAndId() {
    GameManager gameManager = new GameManager();

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("1", "a", "b"));
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("2", "b", "a"));

    Assertions.assertEquals(
        GameManager.OperationStatus.NOT_PARTICIPANT,
        gameManager.doMove("notParticipant", "1", "e4"));
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.doMove("a", "1", "e4"));
    Assertions.assertTrue(gameManager.getGame("a", "1").getSecond().isTurnOf("b"));

    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getGameById("1").getFirst());
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.getGameById("2").getFirst());
    Assertions.assertEquals(GameManager.OperationStatus.SUCCESS, gameManager.dropGame("a", "1"));
  }

  @Test
  public void testWrongMove() {
    GameManager gameManager = new GameManager();
    Assertions.assertEquals(
        GameManager.OperationStatus.SUCCESS, gameManager.addGame("1", "a", "b"));
    Assertions.assertEquals(
        GameManager.OperationStatus.MOVE_ILLEGAL, gameManager.doMove("a", "lol"));
  }
}
