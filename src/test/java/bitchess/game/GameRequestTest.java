package bitchess.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for {@link GameRequest}. */
public class GameRequestTest {

  @Test
  public void test() {
    GameRequest gameRequest1 = new GameRequest("a", "b");
    GameRequest gameRequest2 = new GameRequest("a", "b");
    GameRequest gameRequest3 = new GameRequest("a", "c");
    GameRequest gameRequest4 = new GameRequest("b", "a");

    GameRequest gameRequest5 = new GameRequest("a");
    GameRequest gameRequest6 = new GameRequest("a");
    GameRequest gameRequest7 = new GameRequest("b");

    Assertions.assertFalse(gameRequest1.isPublic());
    Assertions.assertFalse(gameRequest2.isPublic());
    Assertions.assertFalse(gameRequest3.isPublic());
    Assertions.assertFalse(gameRequest4.isPublic());
    Assertions.assertTrue(gameRequest5.isPublic());
    Assertions.assertTrue(gameRequest6.isPublic());
    Assertions.assertTrue(gameRequest7.isPublic());

    Assertions.assertEquals(gameRequest1, gameRequest2);
    Assertions.assertEquals(gameRequest1.toString(), gameRequest2.toString());
    Assertions.assertEquals(gameRequest1.hashCode(), gameRequest2.hashCode());

    Assertions.assertNotEquals(gameRequest1, gameRequest3);
    Assertions.assertNotEquals(gameRequest1.toString(), gameRequest3.toString());
    Assertions.assertNotEquals(gameRequest1.hashCode(), gameRequest3.hashCode());

    Assertions.assertNotEquals(gameRequest1, gameRequest4);
    Assertions.assertNotEquals(gameRequest1.toString(), gameRequest4.toString());
    Assertions.assertNotEquals(gameRequest1.hashCode(), gameRequest4.hashCode());

    Assertions.assertEquals(gameRequest5, gameRequest6);
    Assertions.assertEquals(gameRequest5.toString(), gameRequest6.toString());
    Assertions.assertEquals(gameRequest5.hashCode(), gameRequest6.hashCode());

    Assertions.assertNotEquals(gameRequest5, gameRequest7);
    Assertions.assertNotEquals(gameRequest5.toString(), gameRequest7.toString());
    Assertions.assertNotEquals(gameRequest5.hashCode(), gameRequest7.hashCode());

    Assertions.assertNotEquals(gameRequest1, gameRequest5);
    Assertions.assertNotEquals(gameRequest1.toString(), gameRequest5.toString());
    Assertions.assertNotEquals(gameRequest1.hashCode(), gameRequest5.hashCode());
  }
}
