package bitchess.game;

/**
 * A pair of elements.
 *
 * @param <T> the type of the first element
 * @param <U> the type of the second element
 */
public class Pair<T, U> {
  private final T first;
  private final U second;

  /**
   * Constructs a new pair.
   *
   * @param first the first element
   * @param second the second element
   */
  public Pair(T first, U second) {
    this.first = first;
    this.second = second;
  }

  /**
   * Returns the first element.
   *
   * @return the first element
   */
  public T getFirst() {
    return first;
  }

  /**
   * Returns the second element.
   *
   * @return the second element
   */
  public U getSecond() {
    return second;
  }
}
