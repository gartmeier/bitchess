package bitchess.game;

/**
 * Represents a request for a game. can be either private (acceptable by only one target player) or
 * public (acceptable by any player).
 */
public class GameRequest {

  private final String from;

  private final String to;

  /**
   * constructs a new private GameRequest.
   *
   * @param from who is requesting
   * @param to who is being requested
   */
  public GameRequest(String from, String to) {
    this.from = from;
    this.to = to;
  }

  /**
   * constructs a new public GameRequest.
   *
   * @param from who is requesting
   */
  public GameRequest(String from) {
    this.from = from;
    this.to = null;
  }

  /**
   * returns the name of the player who is requesting.
   *
   * @return the name of the player who is requesting
   */
  public String getFrom() {
    return from;
  }

  /**
   * returns the name of the player who is being requested.
   *
   * @return the name of the player who is being requested
   */
  public String getTo() {
    return to;
  }

  /**
   * returns true if the request is public.
   *
   * @return true if the request is public
   */
  public boolean isPublic() {
    return to == null;
  }

  /**
   * returns the string representation of the request.
   *
   * @return the string representation of the request
   */
  public String toString() {
    if (isPublic()) {
      return "GameRequest: " + from;
    } else {
      return "GameRequest: " + from + " " + to;
    }
  }

  /**
   * returns true if the object is equal to this.
   *
   * @param o the object to compare
   * @return true if the object is equal to this
   */
  public boolean equals(Object o) {
    if (o == null || o.getClass() != this.getClass()) {
      return false;
    }
    if (this == o) {
      return true;
    }
    return o.toString().equals(this.toString());
  }

  /**
   * returns the hash code of this.
   *
   * @return the hash code
   */
  public int hashCode() {
    return toString().hashCode();
  }
}
