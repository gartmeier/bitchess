package bitchess.game;

import java.util.HashSet;
import java.util.Set;

/** Manages all game requests. */
public class MatchMaker {

  /** return value of all methods. Indicates the status of the operation. */
  public enum OperationStatus {
    /** when the operation was generally successful. */
    SUCCESS,
    /** when trying to create a request that already exists. */
    ALREADY_EXISTS,

    /** when trying to accept/decline/take back a request that does not exist. */
    NOT_EXISTS,
    /** when trying to request/accept a game with both the same name. */
    FROM_EQUALS_TO,
  }

  private final Set<GameRequest> requests;

  /** Constructs a new match maker. */
  public MatchMaker() {
    this.requests = new HashSet<>();
  }

  /**
   * adds a new private request.
   *
   * @param from who is requesting
   * @param to who is being requested
   * @return the status of the operation
   */
  public OperationStatus privateRequest(String from, String to) {
    if (from.equals(to)) {
      return OperationStatus.FROM_EQUALS_TO;
    }
    GameRequest request = new GameRequest(from, to);
    if (requests.contains(request)) {
      return OperationStatus.ALREADY_EXISTS;
    }
    requests.add(request);
    return OperationStatus.SUCCESS;
  }

  /**
   * adds a new public request.
   *
   * @param from who is requesting
   * @return the status of the operation
   */
  public OperationStatus publicRequest(String from) {
    GameRequest request = new GameRequest(from);
    if (requests.contains(request)) {
      return OperationStatus.ALREADY_EXISTS;
    }
    requests.add(request);
    return OperationStatus.SUCCESS;
  }

  /**
   * accepts a request and removes from the list. only successful if there is a private request from
   * "from" to "to" or a public request from "from". Private requests are picked first.
   *
   * @param from who has requested
   * @param to who is now accepting the request
   * @return the status of the operation
   */
  public OperationStatus accept(String from, String to) {
    if (from.equals(to)) {
      return OperationStatus.FROM_EQUALS_TO;
    }
    GameRequest request = new GameRequest(from, to);
    if (requests.contains(request)) {
      requests.remove(request);
      return OperationStatus.SUCCESS;
    }
    request = new GameRequest(from);
    if (requests.contains(request)) {
      requests.remove(request);
      return OperationStatus.SUCCESS;
    }
    return OperationStatus.NOT_EXISTS;
  }

  /**
   * declines a request and removes from the list. public requests cannot be declined from any
   * player but only taken back.
   *
   * @param from who has requested
   * @param to who is now declining the request
   * @return the status of the operation
   */
  public OperationStatus decline(String from, String to) {
    if (from.equals(to)) {
      return OperationStatus.FROM_EQUALS_TO;
    }
    GameRequest request = new GameRequest(from, to);
    if (requests.contains(request)) {
      requests.remove(request);
      return OperationStatus.SUCCESS;
    }
    return OperationStatus.NOT_EXISTS;
  }

  /**
   * takes back a private request and removes from the list.
   *
   * @param from who has requested and also now taking back the request
   * @param to who was the target of the request
   * @return the status of the operation
   */
  public OperationStatus takeBackPrivate(String from, String to) {
    if (from.equals(to)) {
      return OperationStatus.FROM_EQUALS_TO;
    }
    GameRequest request = new GameRequest(from, to);
    if (requests.contains(request)) {
      requests.remove(request);
      return OperationStatus.SUCCESS;
    }
    return OperationStatus.NOT_EXISTS;
  }

  /**
   * takes back a public request and removes from the list.
   *
   * @param from who has requested and now taking back the request
   * @return the status of the operation
   */
  public OperationStatus takeBackPublic(String from) {
    GameRequest request = new GameRequest(from);
    if (requests.contains(request)) {
      requests.remove(request);
      return OperationStatus.SUCCESS;
    }
    return OperationStatus.NOT_EXISTS;
  }
}
