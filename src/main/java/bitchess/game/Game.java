package bitchess.game;

import bitchess.visual.BoardVisualizer;
import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.move.MoveConversionException;

/**
 * Copyright 2023 Ben-Hur Carlos Vieira Langoni Junior
 *
 * <p>Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * <p><a href="http://www.apache.org/licenses/LICENSE-2.0">...</a>
 *
 * <p>Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * <p>I am not sure if I used the copyright correctly. Thanks anyway to the developers from
 *
 * <p><a href="https://github.com/bhlangonijr/chesslib">...</a>
 *
 * <p>for the chesslib library which I used in this class.
 *
 * <p>class that contains the board, checks moves and executes them.
 */
public class Game {

  /** in which status the game is. */
  public enum GameStatus {
    /** the game is running. */
    RUNNING,
    /** the game is over and white won. */
    WHITE_WIN,

    /** the game is over and black won. */
    BLACK_WIN,
    /** the game is over, and it's a draw (insufficient material or 50 moves rule). */
    DRAW,
    /** the game is over because of position repetition. */
    REPETITION,
    /** the game is over, and it's a draw (stalemate). */
    STALEMATE
  }

  private final String id;
  private final String whitePlayer;
  private final String blackPlayer;

  private final Board board;

  /**
   * constructs a new game from the starting position.
   *
   * @param id the id of the game
   * @param whitePlayer the name of the white player
   * @param blackPlayer the name of the black player
   */
  public Game(String id, String whitePlayer, String blackPlayer) {
    this.id = id;
    this.whitePlayer = whitePlayer;
    this.blackPlayer = blackPlayer;
    this.board = new Board();
  }

  /**
   * constructs a new game from the given FEN.
   *
   * @param id th id of the game
   * @param whitePlayer the name of the white player
   * @param blackPlayer the name of the black player
   * @param fen the FEN that contains the position to start from
   */
  public Game(String id, String whitePlayer, String blackPlayer, String fen) {
    this.id = id;
    this.whitePlayer = whitePlayer;
    this.blackPlayer = blackPlayer;
    this.board = new Board();
    board.loadFromFen(fen);
  }

  /**
   * returns a printable ascii representation of the board.
   *
   * @return the ascii representation of the board.
   */
  public String getAsciiBoard() {
    return BoardVisualizer.fenToAsciiBoard(board.getFen());
  }

  /**
   * returns a printable ascii representation of the board from the given perspective.
   *
   * @param perspective from which perspective (which player is at the bottom)
   * @return the ascii representation of the board.
   */
  public String getAsciiBoard(BoardVisualizer.Perspective perspective) {
    return BoardVisualizer.fenToAsciiBoard(board.getFen(), perspective);
  }

  /**
   * returns the FEN representation of the current position.
   *
   * @return the FEN representation of the current position.
   */
  public String getFen() {
    return board.getFen();
  }

  /**
   * executes the given move in SAN notation.
   *
   * @param move the move to execute
   * @return true if the move was executed successfully. Returns false if the move was illegal or
   *     the SAN syntax is not valid.
   */
  public boolean doMove(String move) {
    try {
      board.doMove(move);
      return true;
    } catch (MoveConversionException e) {
      return false;
    }
  }

  /**
   * returns the id of the game.
   *
   * @return the id of the game
   */
  public String getId() {
    return id;
  }

  /**
   * returns the name of the white player.
   *
   * @return the name of the white player
   */
  public String getWhitePlayer() {
    return whitePlayer;
  }

  /**
   * returns the name of the black player.
   *
   * @return the name of the black player
   */
  public String getBlackPlayer() {
    return blackPlayer;
  }

  /**
   * returns the status of the game.
   *
   * @return the status of the game
   */
  public GameStatus getGameStatus() {
    if (board.isRepetition()) {
      return GameStatus.REPETITION;
    }
    if (board.isStaleMate()) {
      return GameStatus.STALEMATE;
    }
    if (board.isMated()) {
      if (board.getSideToMove() == Side.WHITE) {
        return GameStatus.BLACK_WIN;
      } else {
        return GameStatus.WHITE_WIN;
      }
    }
    if (board.isDraw()) {
      return GameStatus.DRAW;
    }
    return GameStatus.RUNNING;
  }

  /**
   * returns true if it's the player's turn.
   *
   * @param player the name of the player
   * @return true if it's the player's turn
   */
  public boolean isTurnOf(String player) {
    return board.getSideToMove() == Side.WHITE
        ? whitePlayer.equals(player)
        : blackPlayer.equals(player);
  }

  /**
   * returns true if the player is a participant of the game (either white or black).
   *
   * @param player the name of the player
   * @return true if the player is a participant of the game
   */
  public boolean isParticipant(String player) {
    return whitePlayer.equals(player) || blackPlayer.equals(player);
  }

  /**
   * returns the string representation of the game.
   *
   * @return the string representation of the game
   */
  public String toString() {
    return "Game: " + id + " " + whitePlayer + " " + blackPlayer + " " + board.getFen();
  }
}
