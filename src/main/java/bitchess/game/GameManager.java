package bitchess.game;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/** Manages multiple games. All actions can be made with a player's name and/or game id. */
public class GameManager {

  /** retturn values of all methods. Indicates the status of the operation. */
  public enum OperationStatus {
    /** when the operation was generally successful. */
    SUCCESS,
    /** when there is no game that fits the player's namme and/or game id. */
    NO_GAME,
    /**
     * when there are multiple games that fits the player's name. Does not occur when game id is
     * used because it is not possible to have multiple games with the same id.
     */
    MULTIPLE_GAMES,
    /** when trying to do a move when it is not the player's turn. */
    NOT_YOUR_TURN,
    /** when trying to interfere in a game that the player is not participating in. */
    NOT_PARTICIPANT,
    /** when trying to do a move that is illegal or wrong SAN syntax. */
    MOVE_ILLEGAL,
  }

  private final Set<Game> games;

  /** constructs a new game manager. */
  public GameManager() {
    this.games = new HashSet<>();
  }

  /**
   * makes a given move. Only successful if the move is legal, the player's turn and there is
   * exactly one game that the player is participating in.
   *
   * @param player the name of the player who makes the move
   * @param move the move to make in SAN notation
   * @return the status of the operation
   */
  public OperationStatus doMove(String player, String move) {
    List<Game> participatingGames =
        games.stream().filter(game -> game.isParticipant(player)).toList();

    if (participatingGames.size() == 0) {
      return OperationStatus.NO_GAME;
    }

    if (participatingGames.size() > 1) {
      return OperationStatus.MULTIPLE_GAMES;
    }

    Game onlyGame = participatingGames.get(0);

    if (!onlyGame.isTurnOf(player)) {
      return OperationStatus.NOT_YOUR_TURN;
    }

    if (onlyGame.doMove(move)) {
      return OperationStatus.SUCCESS;
    } else {
      return OperationStatus.MOVE_ILLEGAL;
    }
  }

  /**
   * makes a given move. Only successful if the move is legal, the player's turn and the game with
   * the given id and player exists.
   *
   * @param player the name of the player who makes the move
   * @param id the id of the game
   * @param move the move to make in SAN notation
   * @return the status of the operation
   */
  public OperationStatus doMove(String player, String id, String move) {
    Game onlyGame = games.stream().filter(game -> game.getId().equals(id)).findFirst().orElse(null);

    if (onlyGame == null) {
      return OperationStatus.NO_GAME;
    }

    if (!onlyGame.isParticipant(player)) {
      return OperationStatus.NOT_PARTICIPANT;
    }
    if (!onlyGame.isTurnOf(player)) {
      return OperationStatus.NOT_YOUR_TURN;
    }

    if (onlyGame.doMove(move)) {
      return OperationStatus.SUCCESS;
    } else {
      return OperationStatus.MOVE_ILLEGAL;
    }
  }

  /**
   * drops a game. Only successful if the player is participating in exactly one game (which is
   * dropped).
   *
   * @param player the name of the player
   * @return the status of the operation
   */
  public OperationStatus dropGame(String player) {
    List<Game> participatingGames =
        games.stream().filter(game -> game.isParticipant(player)).toList();
    if (participatingGames.size() == 0) {
      return OperationStatus.NO_GAME;
    }
    if (participatingGames.size() > 1) {
      return OperationStatus.MULTIPLE_GAMES;
    }
    Game onlyGame = participatingGames.get(0);
    games.remove(onlyGame);
    return OperationStatus.SUCCESS;
  }

  /**
   * drops a game. Only successful if the game with the given id and player exists.
   *
   * @param player the name of the player
   * @param id the id of the game
   * @return the status of the operation
   */
  public OperationStatus dropGame(String player, String id) {
    Game onlyGame = games.stream().filter(game -> game.getId().equals(id)).findFirst().orElse(null);
    if (onlyGame == null) {
      return OperationStatus.NO_GAME;
    }
    if (!onlyGame.isParticipant(player)) {
      return OperationStatus.NOT_PARTICIPANT;
    }
    games.remove(onlyGame);
    return OperationStatus.SUCCESS;
  }

  /**
   * adds a new game. Only successful if there is no game with the same id.
   *
   * @param id the id of the game
   * @param whitePlayer the name of the white player
   * @param blackPlayer the name of the black player
   * @return the status of the operation
   */
  public OperationStatus addGame(String id, String whitePlayer, String blackPlayer) {
    if (games.stream().anyMatch(game -> game.getId().equals(id))) {
      return OperationStatus.MULTIPLE_GAMES;
    }
    Game game = new Game(id, whitePlayer, blackPlayer);
    games.add(game);
    return OperationStatus.SUCCESS;
  }

  /**
   * returns the game with the given player. Only successful if there is exactly one game that the
   * player is participating in.
   *
   * @param player the name of the player
   * @return the status of the operation and the game
   */
  public Pair<OperationStatus, Game> getGameByPlayer(String player) {
    List<Game> participatingGames =
        games.stream().filter(game -> game.isParticipant(player)).toList();
    if (participatingGames.size() == 0) {
      return new Pair<>(OperationStatus.NO_GAME, null);
    }
    if (participatingGames.size() > 1) {
      return new Pair<>(OperationStatus.MULTIPLE_GAMES, null);
    }
    Game onlyGame = participatingGames.get(0);
    return new Pair<>(OperationStatus.SUCCESS, onlyGame);
  }

  /**
   * returns the game with the given id. Only successful if there is exactly one game with the given
   * id and player.
   *
   * @param player the name of the player
   * @param id the id of the game
   * @return the status of the operation and the game
   */
  public Pair<OperationStatus, Game> getGame(String player, String id) {
    Game onlyGame = games.stream().filter(game -> game.getId().equals(id)).findFirst().orElse(null);
    if (onlyGame == null) {
      return new Pair<>(OperationStatus.NO_GAME, null);
    }
    if (!onlyGame.isParticipant(player)) {
      return new Pair<>(OperationStatus.NOT_PARTICIPANT, null);
    }
    return new Pair<>(OperationStatus.SUCCESS, onlyGame);
  }

  /**
   * returns the game with the given id. Only successful if the game with the given id exists.
   *
   * @param id the id of the game
   * @return the status of the operation
   */
  public Pair<OperationStatus, Game> getGameById(String id) {
    Game onlyGame = games.stream().filter(game -> game.getId().equals(id)).findFirst().orElse(null);
    if (onlyGame == null) {
      return new Pair<>(OperationStatus.NO_GAME, null);
    }
    return new Pair<>(OperationStatus.SUCCESS, onlyGame);
  }

  /**
   * returns a set of all ids of all games that the player is participating in. Only Successful if
   * there is at least one.
   *
   * @param player the name of the player
   * @return the status of the operation and the set of ids
   */
  public Pair<OperationStatus, Set<String>> getIdsByPlayer(String player) {
    Set<String> ids =
        games.stream()
            .filter(game -> game.isParticipant(player))
            .map(Game::getId)
            .collect(Collectors.toSet());
    if (ids.size() == 0) {
      return new Pair<>(OperationStatus.NO_GAME, null);
    }
    return new Pair<>(OperationStatus.SUCCESS, ids);
  }

  /**
   * returns a set of all strings of all games that the player is participating in. Only Successful
   * if there is at least one.
   *
   * @param player the name of the player
   * @return the status of the operation and the set of strings
   */
  public Pair<OperationStatus, Set<String>> getGameStringsByPlayer(String player) {
    Set<String> strs =
        games.stream()
            .filter(game -> game.isParticipant(player))
            .map(Game::toString)
            .collect(Collectors.toSet());
    if (strs.size() == 0) {
      return new Pair<>(OperationStatus.NO_GAME, null);
    }
    return new Pair<>(OperationStatus.SUCCESS, strs);
  }
}
