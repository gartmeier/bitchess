package bitchess.bot;

/** only a dummy class. */
public class Main {

  private Main() {}

  /**
   * The famous main method.
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {
    System.out.println("Hello World!");
  }
}
