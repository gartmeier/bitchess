package bitchess.visual;

/**
 * This class is used to convert a [FEN](<a
 * href="https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation">...</a>) into a visual
 * board.* Currently only an ascii representation is supported. An Image representation is planned.
 */
public class BoardVisualizer {

  /** The perspective of the board (which side of the board is at the bottom). */
  public enum Perspective {
    /** White's perspective. */
    WHITE,
    /** Black's perspective. */
    BLACK,
    /** The perspective of the current player. */
    CURRENT_PLAYER
  }

  /** The default perspective. */
  public static final Perspective DEFAULT_PERSPECTIVE = Perspective.WHITE;

  private static final String horizontalLine;
  private static final String columnIndexLine;
  private static final String flippedColumnIndexLine;
  private static final String[] lineIndexEndings;
  private static final String emptySquare;

  private static final String nextPieceLine;

  static {
    horizontalLine = "+----+----+----+----+----+----+----+----+";
    columnIndexLine = "  a    b    c    d    e    f    g    h";
    flippedColumnIndexLine = "  h    g    f    e    d    c    b    a";
    lineIndexEndings = new String[] {"8", "7", "6", "5", "4", "3", "2", "1"};
    emptySquare = "    |";
    nextPieceLine = "\n" + horizontalLine + "\n|";
  }

  /** The default constructor. Useless because all methods are static. */
  public BoardVisualizer() {}

  /**
   * Converts a [FEN](<a
   * href="https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation">...</a>) representation
   * of a board into an ascii representation from White's perspective (white is at the bottom).
   *
   * @param fen the FEN representation of the board
   * @return the ascii representation of the board with no new line at the end.
   */
  public static String fenToAsciiBoard(String fen) {
    return fenToAsciiBoard(fen, DEFAULT_PERSPECTIVE);
  }

  /**
   * Converts a [FEN](<a
   * href="https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation">...</a>) representation
   * of a board into an ascii representation.
   *
   * @param fen the FEN representation of the board
   * @param perspective from which perspective the board is (who is at the bottom)
   * @return the ascii representation of the board
   */
  public static String fenToAsciiBoard(String fen, Perspective perspective) {
    String position = fen.split(" ")[0];

    if (perspective == Perspective.WHITE) {
      return asciiBoardWhitePerspective(position);
    } else if (perspective == Perspective.BLACK) {
      return asciiBoardBlackPerspective(position);
    } else {
      if (fen.split(" ")[1].equals("w")) {
        return asciiBoardWhitePerspective(position);
      } else {
        return asciiBoardBlackPerspective(position);
      }
    }
  }

  private static String asciiBoardWhitePerspective(String position) {
    StringBuilder board = new StringBuilder(horizontalLine).append("\n|");
    int lineIndex = 0;
    for (char c : position.toCharArray()) {
      try {
        int emptySquares = Integer.parseInt(String.valueOf(c));
        board.append(emptySquare.repeat(emptySquares));
      } catch (NumberFormatException e) {
        if (c == '/') {
          board.append("  ").append(lineIndexEndings[lineIndex]).append(nextPieceLine);
          lineIndex++;
        } else {
          board.append(asciiSquareWith(c));
        }
      }
    }
    board
        .append(" ")
        .append(lineIndexEndings[lineIndex])
        .append("\n")
        .append(horizontalLine)
        .append("\n")
        .append(columnIndexLine)
        .append("\n");
    return board.toString();
  }

  private static String asciiBoardBlackPerspective(String position) {
    StringBuilder board = new StringBuilder(horizontalLine).append("\n|");
    int lineIndex = 7;
    for (char c : new StringBuilder(position).reverse().toString().toCharArray()) {
      try {
        int emptySquares = Integer.parseInt(String.valueOf(c));
        board.append(emptySquare.repeat(emptySquares));
      } catch (NumberFormatException e) {
        if (c == '/') {
          board.append("  ").append(lineIndexEndings[lineIndex]).append(nextPieceLine);
          lineIndex--;
        } else {
          board.append(asciiSquareWith(c));
        }
      }
    }
    board
        .append(" ")
        .append(lineIndexEndings[lineIndex])
        .append("\n")
        .append(horizontalLine)
        .append("\n")
        .append(flippedColumnIndexLine)
        .append("\n");
    return board.toString();
  }

  private static String asciiSquareWith(char c) {
    return " " + c + "  |";
  }
}
