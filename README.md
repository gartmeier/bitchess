# BitChess

This is a hobby project of [Lorenz Gartmeier](https://gitlab2.cip.ifi.lmu.de/gartmeier)

The goal is to implement a discord chess bot which lets members play chess via text arguments
